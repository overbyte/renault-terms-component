/**
 * Renault Terms Component
 * @author Allandt Bik-Elliott
 */

import mx.core.UIObject;

[Event("change")]
class com.publicislondon.components.TermsComponent extends UIObject
{
	// onstage
	public var mcBoundingBox	: MovieClip;
	
	public static var symbolName:String = "om.publicislondon.components.TermsComponent";
	public static var symbolOwner:Object = com.publicislondon.components.TermsComponent;
	public var className:String = "TermsComponent";
	
	private var _text		: String;
	private var _nInset		: Number;
	private var _tf			: TextField; 
	private var _nWidth		: Number;
	private var _nHeight	: Number;
	
	private var _mcTf		: MovieClip;
	private var _mcMask		: MovieClip;
	private var _mcBg		: MovieClip;
	
	public function TermsComponent() 
	{
		
	}
	
	function init():Void
	{
		trace("init");
		super.init();
		
		_nWidth = width;
		_nHeight = height;
		
		mcBoundingBox._width = 0;
		mcBoundingBox._height = 0;
		mcBoundingBox._visible = false;
	}
	
	function createChildren():Void
	{
		trace("createChildren");
		// Call createClassObject to create subobjects.
		
		// create background
		beginFill(0xFFFFFF, 100);
		drawRect(0,0,width,height);
		endFill();
		
		createObject("mcTf", "_mcTf", getNextHighestDepth());
		createObject("mcMask", "_mcMask", getNextHighestDepth());
		
		_mcTf.setMask(_mcMask);
		
		_tf = _mcTf["tf"];
		_tf.text = _text;
		
		size();
	}

	function size():Void
	{
		trace("size");
		super.size();
		
		// Write code to handle sizing.
		setSize(_width, _height);
		
		invalidate();
	}

	function draw():Void
	{
		super.draw();
		
		arrange();
		
		trace("draw");
	}
	
	private function setSize(nW:Number, nH:Number):Void 
	{
		trace("setSize");
		_xscale = 100;
		_yscale = 100;
		
		if (nW != null) 
		{
			_nWidth = nW;
		}
		if (nH != null) 
		{
			_nHeight = nH;
		}
	}
	
	private function arrange():Void
	{
		trace("arrange");
		trace("inset" + _nInset);
		_tf._width = _nWidth - (2 * _nInset);
		_mcMask._width = _nWidth - (2 * _nInset);
		_mcMask._height = _nHeight - (2 * _nInset);
		_mcMask._x = _nInset;
		_mcMask._y = _nInset;
		_tf._x = _nInset;
		_tf._y = _nInset;
	}
	
	[Bindable]
	[ChangeEvent("change")]
	[Inspectable(defaultValue="Terms & Conditions apply.")]
	public function get text():String 
	{
		return _text;
	}
	public function set text(value:String):Void 
	{
		_text = value;
		_tf.text = _text;
		trace("changing text to " + value);
		trace(_tf);
	}
	
	[Bindable]
	[ChangeEvent("change")]
	[Inspectable(defaultValue=10)]
	public function get inset():Number 
	{
		return _nInset;
	}
	public function set inset(value:Number):Void 
	{
		_nInset = value;
		
		invalidate();
	}
	
}